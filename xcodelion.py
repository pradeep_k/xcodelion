#!/usr/bin/env python2.7
# dep : xmltodict

import sys, os.path
import argparse
import io
import plistlib
import xmltodict
import itertools
from json import loads, dumps

class lion():

    # cmline
    def main(self):
        parser = argparse.ArgumentParser(prog="XCodeLion", description='Plist update tool')

        # main commands
        parser.add_argument("source", metavar='SOURCE FILE', help="source plist file")
        parser.add_argument("merge", metavar='MERGE FILE', help="file with merge data")
        parser.add_argument("--xml", action='store_true', help="Merge data is xml")
        parser.add_argument("--debug", action='store_true', help="Show Dicts Debug")

        parser.add_argument("--target", metavar='TARGET FILE', help="(optional) target file to write to")

        args = parser.parse_args()

        new_path = args.merge

        src_path = args.source
        src_data = self.GetDictFromPlist(src_path)

        merged_dict = None
        new_data    = None
        if args.xml:
            print "Merge File type: xml"
            new_data = self.GetDictFromXML(new_path)
            merged_dict = self.mergeDict(new_data['LionAmend'], src_data)
        else:
            print "Merge File type: defaulting to  plist"
            new_data = self.GetDictFromPlist(new_path)
            merged_dict = self.mergeDict(new_data, src_data)

        if args.debug:
            print "\n merge dict: "
            print new_data
            print "\n source dict: "
            print src_data
            print "\n final merged dict: "
            print merged_dict

        target_path = args.target
        if target_path is None:
            target_path = src_path

        try:
            plistlib.writePlist(merged_dict, target_path)

        except IOError as (errno, strerror):
            raise Exception("IOError: Unable to write plist file, check file name or path access. PATH: " + target_path)

        except:
            raise Exception("Unexpected error: ", sys.exc_info()[0])

        print "Plist updated successfully"

    # core
    def GetDictFromPlist(self, filepath):
        try:
            print "Opening plist:  " +filepath
            data_dict = plistlib.readPlist(filepath)

        except IOError as (errno, strerror):
            raise Exception("IOError: Unable to read plist file, check file name or path access. PATH: " +filepath)

        except:
            raise Exception("Unexpected error: ", sys.exc_info()[0])

        return data_dict

    def GetDictFromXML(self, filepath):

        xmlfileobject = None

        try:
            xmlfileobject = io.open(filepath,mode='r',encoding='utf-8')

        except IOError as (errno, strerror):
            raise "IOError: Unable to write plist file, check file name or path access"

        except:
            raise Exception("Unexpected error: ", sys.exc_info()[0])

        ordered_dict = xmltodict.parse(xmlfileobject.read(), encoding='utf-8')
        data_dict = self.to_dict(ordered_dict)
        data_dict = self.convertToAscii(data_dict) #remove unicode marker
        xmlfileobject.close()

        return data_dict

    def to_dict(self, input_ordered_dict):
        return loads(dumps(input_ordered_dict))

    def convertToAscii(self, input):
        if isinstance(input, dict):
            return {self.convertToAscii(key): self.convertToAscii(value) for key, value in input.iteritems()}
        elif isinstance(input, list):
            return [self.convertToAscii(element) for element in input]
        elif isinstance(input, unicode):
            return input.encode('utf-8')
        else:
            return input

    def mergeDict(self, new_dict, src_dict):
        merged_dict = dict(itertools.chain(src_dict.iteritems(), new_dict.iteritems()))
        return  merged_dict


# cmd entry point
if __name__ == "__main__":
    l = lion()
    l.main()

# test case

# cmd
    # python xcodelion.py "Info.plist" "Info_Append.xml" --xml --target "info_new.plist"
    # python xcodelion.py "Info.plist" "Info_Append.plist" --target "info_new.plist"



# install_path = sys.argv[1]
# xml_path     = sys.argv[2]

# TEST_PATH = "/Users/pradeepk/git/Ali/Dev-ali-game-client-iOS5.3.5p7-repo2/Build/QACDN103_21Oct16/Info.plist"
# TEST_PATH = "Info.plist"
# TEST_XML  = "Info_Append.xml"
# install_path = TEST_PATH
#
# src_data = None
# src_data = GetDictFromPlist(TEST_PATH)
# new_data = GetDictFromXML(TEST_XML)
# merged_dict = mergeDict(new_data['LionAmend'], src_data)
#
# print new_data
# print src_data
# print merged_dict